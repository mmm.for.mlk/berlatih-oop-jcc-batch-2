<?php

function pagar_bintang($integer)
{
    for ($i = 1; $i <= $integer; $i++) {
        for ($j = 1; $j <= $integer; $j++) {
            if ($i % 2 == 0) {
                echo "*";
            } else {
                echo "#";
            }
        }

        echo "<br>";
    }
    echo "<br>";
}


echo pagar_bintang(5);
echo pagar_bintang(8);
echo pagar_bintang(10);


function xo($str)
{

    if (substr_count($str, "x") == substr_count($str, "o")) {
        echo "Benar";
    } else echo "Salah";
    echo "<br>";
}

// Test Cases
echo xo('xoxoxo'); // "Benar"
echo xo('oxooxo'); // "Salah"
echo xo('oxx'); // "Salah"
echo xo('xxooox'); // "Benar"
echo xo('xoxooxxo'); // "Benar"
